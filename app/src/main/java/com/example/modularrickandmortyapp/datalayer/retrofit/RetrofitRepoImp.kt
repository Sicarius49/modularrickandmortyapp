package com.example.modularrickandmortyapp.datalayer.retrofit

import com.example.modularrickandmortyapp.datalayer.dto.AllCharactersDTO
import com.example.modularrickandmortyapp.datalayer.dto.SingleCharacterDTO
import com.example.modularrickandmortyapp.datalayer.interfaces.RickAndMortyDataRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class RetrofitRepoImp: RickAndMortyDataRepository {
    private val networkClient = RetrofitClient().api

//    private val ktorClient = Greeting()

    suspend fun getKtorChararcersForPage(page: Int) {

    }

    override suspend fun getAllCharactersForPage(page: String): AllCharactersDTO {
        return withContext(Dispatchers.IO) {
            return@withContext networkClient.getAllCharacters(page)
        }
    }

    override suspend fun getFilteredCharactersByName(name: String): AllCharactersDTO {
        return withContext(Dispatchers.IO) {
            return@withContext networkClient.searchForCharacters(name)
        }
    }

    override suspend fun getSingleCharacterByID(id: Long): SingleCharacterDTO {
        return withContext(Dispatchers.IO) {
            return@withContext networkClient.getCharacterByID(id)
        }
    }
}