package com.example.modularrickandmortyapp.datalayer.dto

import com.google.gson.annotations.SerializedName

data class AllCharactersDTO(
    @SerializedName("info")
    val info: Info,

    @SerializedName("results")
    val results: List<SingleCharacterDTO>
) {
    data class Info(
        @SerializedName("count")
        val count: Long?,

        @SerializedName("pages")
        val pages: Long?,

        @SerializedName("next")
        val next: String?,

        @SerializedName("prev")
        val prev: String?
    )
}