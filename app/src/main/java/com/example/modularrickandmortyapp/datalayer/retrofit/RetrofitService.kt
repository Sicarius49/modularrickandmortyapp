package com.example.modularrickandmortyapp.datalayer.retrofit

import retrofit2.http.GET
import com.example.modularrickandmortyapp.datalayer.dto.AllCharactersDTO
import com.example.modularrickandmortyapp.datalayer.dto.SingleCharacterDTO
import retrofit2.http.Path
import retrofit2.http.Query

interface RetrofitService {
    @GET("character")
    suspend fun getAllCharacters(
        @Query("page") page: String
    ): AllCharactersDTO

    @GET("character")
    suspend fun searchForCharacters(
        @Query("name") searchTerm: String
    ): AllCharactersDTO

    @GET("character/{id}")
    suspend fun getCharacterByID(
        @Path("id") id: Long
    ): SingleCharacterDTO
}