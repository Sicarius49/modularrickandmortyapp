package com.example.modularrickandmortyapp.datalayer.dto

import com.google.gson.annotations.SerializedName

data class SingleCharacterDTO(
    @SerializedName("id")
    val id: Long?,

    @SerializedName("name")
    val name: String?,

    @SerializedName("status")
    val status: String?,

    @SerializedName("species")
    val species: String?,

    @SerializedName("type")
    val type: String?,

    @SerializedName("gender")
    val gender: String?,

    @SerializedName("image")
    val image: String?,

    @SerializedName("episode")
    val episodes: List<String>?
)