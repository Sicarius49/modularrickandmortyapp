package com.example.modularrickandmortyapp.datalayer.retrofit

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitClient {
    companion object {
        private const val TIMEOUT: Long = 60
        private const val BASEURL: String = "https://rickandmortyapi.com/api/"
    }

    val api = getClient().create(RetrofitService::class.java)

    fun getClient(): Retrofit {
        val client = OkHttpClient.Builder()

        client.readTimeout(TIMEOUT, TimeUnit.SECONDS)
        client.writeTimeout(TIMEOUT, TimeUnit.SECONDS)

        client.addInterceptor(
            getLoggingInterceptor()
        )

        return Retrofit.Builder()
            .baseUrl(BASEURL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client.build())
            .build()
    }

    private fun getLoggingInterceptor() : Interceptor {
        return HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC)
    }
}