package com.example.modularrickandmortyapp.datalayer.interfaces

import com.example.modularrickandmortyapp.datalayer.dto.AllCharactersDTO
import com.example.modularrickandmortyapp.datalayer.dto.SingleCharacterDTO

interface RickAndMortyDataRepository {

    suspend fun getAllCharactersForPage(page: String): AllCharactersDTO

    suspend fun getFilteredCharactersByName(name: String): AllCharactersDTO

    suspend fun getSingleCharacterByID(id: Long): SingleCharacterDTO
}