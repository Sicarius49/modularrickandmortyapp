package com.example.modularrickandmortyapp.compose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberAsyncImagePainter
import com.example.modularrickandmortyapp.R
import com.example.modularrickandmortyapp.datalayer.dto.SingleCharacterDTO
import com.example.modularrickandmortyapp.viewmodels.CharacterListViewModel

class ComposeUIActivity: ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Surface (
                modifier = Modifier.fillMaxHeight(),
            ) {
                Column() {
                    CharacterListScreen()
                }
            }
        }
    }
}

@Composable
fun CharacterListScreen(viewModel: CharacterListViewModel = viewModel()) {
    val characterData = viewModel.characterData.observeAsState()
    if (characterData.value == null) {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(text = "Loading...")
        }
    } else {
        val character = characterData.value!!
        var text by remember { mutableStateOf(TextFieldValue("")) }
        Column() {
            Row() {
                TextField(
                    value = text,
                    onValueChange = {
                    text = it
                })
                Button(onClick = { /*TODO*/ }) {
                    Image(painter = painterResource(id = R.drawable.ic_mic_black_24dp), contentDescription = "")
                }
                Button(onClick = { 
                    viewModel.searchData(text.text)
                }) {
                    Image(painter = painterResource(id = R.drawable.ic_search), contentDescription = "")
                }
            }
            LazyColumn(modifier = Modifier.weight(1f) ) {
                items(character.results) { item ->
                    CharacterListItem(item)
                }
            }
            Row(
                modifier = Modifier
                    .padding(12.dp)
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Button(onClick = {
                    viewModel.prevPage?.let {
                        viewModel.getData(it)
                    }
                }) {
                    Text(text = "Prev")
                }
                val currentPage: Int = (viewModel.prevPage ?: 0) + 1
                val pageInfo = viewModel.characterData.value
                Text(text = "page $currentPage/${pageInfo?.info?.pages}\n${pageInfo?.info?.count} results")
                Button(onClick = {
                    viewModel.nextPage?.let {
                        viewModel.getData(it)
                    }
                }) {
                    Text(text = "Next")
                }
            }
        }
    }
}

@Composable
fun CharacterListItem(characterData: SingleCharacterDTO) {
    Card(
        modifier = Modifier
            .padding(6.dp)
            .fillMaxWidth()
    ) {
        Row {
            Image(
                painter = rememberAsyncImagePainter(characterData.image),
                contentDescription = null, // TODO add a content description
                modifier = Modifier.size(70.dp).padding(6.dp)
            )
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 12.dp)
            ) {
                Text(
                    text = characterData.name ?: ""
                )
                Text(
                    text = characterData.status ?: ""
                )
                Text(
                    text = characterData.species ?: ""
                )
            }
        }
    }
}