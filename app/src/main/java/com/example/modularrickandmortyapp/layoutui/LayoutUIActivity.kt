package com.example.modularrickandmortyapp.layoutui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.modularrickandmortyapp.databinding.ActivityLayoutUiBinding

class LayoutUIActivity(): AppCompatActivity() {

    private lateinit var binding: ActivityLayoutUiBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLayoutUiBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}