package com.example.modularrickandmortyapp.layoutui.characterdetail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.example.modularrickandmortyapp.R
import com.example.modularrickandmortyapp.databinding.FragmentCharacterDetailBinding
import com.example.modularrickandmortyapp.databinding.FragmentCharacterListBinding
import com.example.modularrickandmortyapp.datalayer.dto.SingleCharacterDTO
import com.example.modularrickandmortyapp.viewmodels.CharacterDetailViewModel
import com.example.modularrickandmortyapp.viewmodels.CharacterListViewModel
import com.squareup.picasso.Picasso

class CharacterDetailFragment : Fragment() {
    private lateinit var binding: FragmentCharacterDetailBinding
    private lateinit var viewModel: CharacterDetailViewModel

    private val args : CharacterDetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCharacterDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this)[CharacterDetailViewModel::class.java]

        viewModel.characterData.observe(viewLifecycleOwner) {
            if (it != null) {
                updateUI(it)
            } else {
                showError()
            }
        }

        viewModel.getData(args.charID)
    }

    private fun updateUI(charData: SingleCharacterDTO) {
        binding.characterLayout.visibility = View.VISIBLE
        binding.textData.visibility = View.GONE

        Picasso.with(requireContext())
            .load(charData.image)
            .into(binding.imageCharacter)

        binding.textName.text = charData.name
        binding.textSpecies.text = charData.species
        binding.textStatus.text = charData.status
        binding.textType.text = charData.type
    }

    private fun showError() {
        binding.characterLayout.visibility = View.GONE
        binding.textData.visibility = View.VISIBLE
        binding.textData.text = getText(R.string.network_error)
    }
}