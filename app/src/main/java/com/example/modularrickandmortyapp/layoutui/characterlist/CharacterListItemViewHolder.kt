package com.example.modularrickandmortyapp.layoutui.characterlist

import android.widget.AdapterView.OnItemClickListener
import androidx.recyclerview.widget.RecyclerView
import com.example.modularrickandmortyapp.databinding.VhCharacterListItemBinding
import com.example.modularrickandmortyapp.datalayer.dto.SingleCharacterDTO
import com.squareup.picasso.Picasso

class CharacterListItemViewHolder(val layoutBinding: VhCharacterListItemBinding): RecyclerView.ViewHolder(layoutBinding.root) {
    fun bind(item: SingleCharacterDTO, onItemClickListener: (id:Long) -> Unit) {

        item.id?.let {
            layoutBinding.root.setOnClickListener {
                onItemClickListener.invoke(item.id)
            }
        }

        Picasso.with(itemView.context)
            .load(item.image)
            .into(layoutBinding.imageCharacter)

        layoutBinding.textName.text = item.name
        layoutBinding.textStatus.text = item.status
        layoutBinding.textSpecies.text = item.species
    }
}