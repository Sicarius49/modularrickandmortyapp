package com.example.modularrickandmortyapp.layoutui.characterlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavArgs
import androidx.navigation.findNavController
import com.example.modularrickandmortyapp.R
import com.example.modularrickandmortyapp.databinding.FragmentCharacterListBinding
import com.example.modularrickandmortyapp.viewmodels.CharacterListViewModel

class CharacterListFragment: Fragment() {

    private lateinit var viewModel: CharacterListViewModel
    private lateinit var binding: FragmentCharacterListBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCharacterListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this)[CharacterListViewModel::class.java]

        val adapter = CharacterListAdapter {
            val action = CharacterListFragmentDirections.actionCharacterListFragmentToCharacterDetailFragment(
                it
            )

            view.findNavController().navigate(
                action
            )
        }
        binding.recyclerView.adapter = adapter

        viewModel.characterData.observe(viewLifecycleOwner) {
            it?.let {
                val currentPage: Int = (viewModel.prevPage ?: 0) + 1
                binding.textPageInfo.text =
                    "page $currentPage/${it.info.pages}\n${it.info.count} results"
                adapter.submitList(it.results)
                setPrevAndNextButtonState()
            }
        }

        binding.buttonSearch.setOnClickListener {
            viewModel.searchData(binding.inputSearch.text.toString())
        }

        binding.buttonPrev.setOnClickListener {
            viewModel.prevPage?.let { prevPage->
                viewModel.getData(prevPage)
            }
        }

        binding.buttonNext.setOnClickListener {
            viewModel.nextPage?.let { nextPage->
                viewModel.getData(nextPage)
            }
        }
    }

    private fun setPrevAndNextButtonState() {
        binding.buttonNext.isEnabled = viewModel.nextPage != null
        binding.buttonPrev.isEnabled = viewModel.prevPage != null
    }
}