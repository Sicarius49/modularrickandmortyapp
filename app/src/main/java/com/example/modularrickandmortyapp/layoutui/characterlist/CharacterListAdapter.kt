package com.example.modularrickandmortyapp.layoutui.characterlist

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.AdapterView.OnItemClickListener
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.modularrickandmortyapp.databinding.VhCharacterListItemBinding
import com.example.modularrickandmortyapp.datalayer.dto.SingleCharacterDTO

class CharacterListAdapter(
    private val onItemClickListener: (id:Long) -> Unit
): ListAdapter<SingleCharacterDTO, CharacterListItemViewHolder>(RecyclerViewDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterListItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = VhCharacterListItemBinding.inflate(
            inflater,
            parent,
            false
        )

        return CharacterListItemViewHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: CharacterListItemViewHolder, position: Int) {
        holder.bind(getItem(position), onItemClickListener)
    }
}

class RecyclerViewDiffCallback : DiffUtil.ItemCallback<SingleCharacterDTO>() {
    override fun areItemsTheSame(oldItem: SingleCharacterDTO, newItem: SingleCharacterDTO): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: SingleCharacterDTO, newItem: SingleCharacterDTO): Boolean {
        return oldItem == newItem
    }
}