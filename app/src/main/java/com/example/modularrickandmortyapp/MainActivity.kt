package com.example.modularrickandmortyapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.example.modularrickandmortyapp.compose.ComposeUIActivity
import com.example.modularrickandmortyapp.layoutui.LayoutUIActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<TextView>(R.id.button_launch_layout_ui).setOnClickListener {
            val intent = Intent(this, LayoutUIActivity::class.java)
            startActivity(intent)
        }

        findViewById<TextView>(R.id.button_launch_compose_ui).setOnClickListener {
            val intent = Intent(this, ComposeUIActivity::class.java)
            startActivity(intent)
        }
    }
}