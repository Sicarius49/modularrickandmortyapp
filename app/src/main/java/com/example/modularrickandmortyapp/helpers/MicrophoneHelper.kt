package com.example.modularrickandmortyapp.helpers

import android.Manifest
import android.app.Activity
import android.content.ContentProvider
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import java.util.*

open class MicrophoneHelperFragment  {
    private val speechRecognizerIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH).apply {
        putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
    }

    fun confirmPermission(context: Context): Boolean {
        return ContextCompat.checkSelfPermission(
            context, Manifest.permission.RECORD_AUDIO
        ) == PackageManager.PERMISSION_GRANTED
    }

//    fun askPermission(
//        fragment: Fragment,
//        onRecordingStarted: () -> Unit,
//        onRecordingDone: (String?) -> Unit
//    ): SpeechRecognizer? {
//        fragment.registerForActivityResult(ActivityResultContracts.RequestPermission()) { result ->
//                return if (result) {
//                    setupMicrophoneFeature(fragment.requireContext(), onRecordingStarted, onRecordingDone)
//                } else {
//                    null//viewModel.showLocationDeniedError()
//                }
//            }
//    }

    private fun setupMicrophoneFeature(
        context: Context,
        onRecordingStarted: () -> Unit,
        onRecordingDone: (String?) -> Unit,
    ): SpeechRecognizer {
        val speechRecognizer = SpeechRecognizer.createSpeechRecognizer(context)
        speechRecognizer.setRecognitionListener(object : RecognitionListener {
            override fun onReadyForSpeech(bundle: Bundle) {}
            override fun onBeginningOfSpeech() {
                onRecordingStarted.invoke()
            }

            override fun onRmsChanged(v: Float) {}
            override fun onBufferReceived(bytes: ByteArray) {}
            override fun onEndOfSpeech() {}
            override fun onError(i: Int) {}
            override fun onResults(bundle: Bundle) {
                val data = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
                onRecordingDone.invoke(data?.firstOrNull())
            }

            override fun onPartialResults(bundle: Bundle) {}
            override fun onEvent(i: Int, bundle: Bundle) {}
        })
        return speechRecognizer
    }
}

