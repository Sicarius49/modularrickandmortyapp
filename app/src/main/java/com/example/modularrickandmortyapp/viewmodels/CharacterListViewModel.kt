package com.example.modularrickandmortyapp.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.modularrickandmortyapp.datalayer.dto.AllCharactersDTO
import com.example.modularrickandmortyapp.datalayer.retrofit.RetrofitRepoImp
import kotlinx.coroutines.launch

class CharacterListViewModel : ViewModel() {

    // TODO: Pass the repo instance via factory
    private val repo = RetrofitRepoImp()

    var prevPage: Int? = null
    var nextPage: Int? = null

    var characterData: MutableLiveData<AllCharactersDTO?> = MutableLiveData()

    init {
        getData()
    }

    fun getData(page: Int = 1) {
        characterData.value = null
        viewModelScope.launch {
            kotlin.runCatching {
                repo.getAllCharactersForPage(page.toString())
            }.onSuccess {
                handleGetCharactersSuccessResponse(it)
            }.onFailure {
                //callback(null)
            }
        }
    }

    fun searchData(term: String) {
        characterData.value = null
        viewModelScope.launch {
            kotlin.runCatching {
                repo.getFilteredCharactersByName(term)
            }.onSuccess {
                handleGetCharactersSuccessResponse(it)
            }.onFailure {
                //callback(null)
                val e = it
            }
        }
    }

    private fun handleGetCharactersSuccessResponse(response: AllCharactersDTO) {
        prevPage = response.info.prev?.substring(
            response.info.prev.indexOf("=") + 1
        )?.toIntOrNull()
        nextPage = response.info.next?.substring(
            response.info.next.indexOf("=") + 1
        )?.toIntOrNull()
        characterData.value = response
    }
}