package com.example.modularrickandmortyapp.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.modularrickandmortyapp.datalayer.dto.AllCharactersDTO
import com.example.modularrickandmortyapp.datalayer.dto.SingleCharacterDTO
import com.example.modularrickandmortyapp.datalayer.retrofit.RetrofitRepoImp
import kotlinx.coroutines.launch

class CharacterDetailViewModel : ViewModel() {

    // TODO: Pass the repo instance via factory
    private val repo = RetrofitRepoImp()
    var characterData: MutableLiveData<SingleCharacterDTO?> = MutableLiveData()

    fun getData(id: Long) {
        characterData.value = null
        viewModelScope.launch {
            kotlin.runCatching {
                repo.getSingleCharacterByID(id)
            }.onSuccess {
                handleGetCharactersSuccessResponse(it)
            }.onFailure {
                characterData.value = null
            }
        }
    }

    private fun handleGetCharactersSuccessResponse(response: SingleCharacterDTO) {
        characterData.value = response
    }
}
